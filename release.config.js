/* eslint-disable no-template-curly-in-string */

const gitlabUrl = 'https://gitlab.com'
const gitlabApiPathPrefix = '/api/v4'
const assets = [
    { path: 'index.js', label: 'JS distribution' }
]

let npmConfig = ["@semantic-release/npm", { // this here is just for package.log update
    "npmPublish": false,
}];
const git = [
    '@semantic-release/git',
    {
        assets: ['package.json', 'CHANGELOG.md', 'chart/*.yaml'],
        message: 'chore(release): ${nextRelease.version}\n\n${nextRelease.notes}'
    }
];

const verifyConditions = [
    ['@semantic-release/changelog'],
    npmConfig,
    git,
    [
        '@semantic-release/gitlab',
        {
            gitlabUrl,
            assets
        }
    ]
]
const analyzeCommits = [[
    '@semantic-release/commit-analyzer',
]]

const generateNotes = ['@semantic-release/release-notes-generator']
const prepare = [
    '@semantic-release/changelog',
    ['@semantic-release/exec',
    {
        "prepareCmd": "test -f chart/Chart.yaml && sed -i -e '/appVersion\:/ s/: .*/: \"${nextRelease.version}\"/' chart/Chart.yaml || echo nochart"
    }],
    npmConfig,
    git
]
const publish = [
    npmConfig,
    ['@semantic-release/gitlab',
        {
            gitlabUrl,
            gitlabApiPathPrefix,
            assets
        }]
    
]

// skipped steps
const verifyRelease = []
const fail = []
const success = []
const addChannel = []

// eslint-disable-next-line no-undef
module.exports = {
    tagFormat: "${version}",
    analyzeCommits,
    generateNotes,
    verifyConditions,
    verifyRelease,
    prepare,
    publish,
    fail,
    success,
    addChannel
}
